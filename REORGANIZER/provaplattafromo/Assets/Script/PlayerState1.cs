﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerState1 : MonoBehaviour {


    public static int Playerstat1;

    public Color Attraction;
    public Color Repulsion;
    public Color Initial;

    Renderer PL;

    // Use this for initialization
    void Start()
    {
        Playerstat1 = 0;
    }

    void stat()
    {
        if (Playerstat1 == 1)
        {
            PL.material.color = Attraction;
        }
        if (Playerstat1 == 2)
        {
            PL.material.color = Repulsion;
        }
        if (Playerstat1 == 0)
        {
            PL.material.color = Initial;
        }
    }
    // Update is called once per frame
    void FixedUpdate()
    {

        if (Input.GetKey(KeyCode.F))
        {
            PL.material.color = Attraction;
            Playerstat1 = 1;

        }
        if (Input.GetKey(KeyCode.C))
        {
            PL.material.color = Repulsion;
            Playerstat1 = 2;
        }
        if (Input.GetKey(KeyCode.R))
        {
            PL.material.color = Initial;
            Playerstat1 = 0;
        }
    }
}
