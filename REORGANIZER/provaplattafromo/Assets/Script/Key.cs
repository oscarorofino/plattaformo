﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Key : MonoBehaviour {

    public bool key;

    void Start()
    {
        key = false;
    }
    void OnTriggerEnter(Collider collision)
    {
        DestroyObject(gameObject);
        key = true;
    }
}
