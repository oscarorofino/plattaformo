﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerState2 : MonoBehaviour {


    public static int Playerstat2;

    public Color Attraction;
    public Color Repulsion;
    public Color Initial;

    Renderer PL;

    // Use this for initialization
    void Start()
    {
        Playerstat2 = 0;
    }

    void stat()
    {
        if (Playerstat2 == 1)
        {
            PL.material.color = Attraction;
        }
        if (Playerstat2 == 2)
        {
            PL.material.color = Repulsion;
        }
        if (Playerstat2 == 0)
        {
            PL.material.color = Initial;
        }
    }
    // Update is called once per frame
    void FixedUpdate()
    {

        if (Input.GetKey(KeyCode.F))
        {
            PL.material.color = Attraction;
            Playerstat2 = 1;

        }
        if (Input.GetKey(KeyCode.C))
        {
            PL.material.color = Repulsion;
            Playerstat2 = 2;
        }
        if (Input.GetKey(KeyCode.R))
        {
            PL.material.color = Initial;
            Playerstat2 = 0;
        }
    }
}
