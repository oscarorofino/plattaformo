﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement1 : MonoBehaviour {

    [SerializeField]
    public float speedLeft1;
    public float speedRight1;
    public bool ismoving1;
    public bool freeze = false;
    Rigidbody rb;
    void Start()
    {
        freezePosition();
        rb = GetComponent<Rigidbody>();
    }


    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.D))
        {
            transform.Translate(Vector3.right * speedRight1);
        }
        if (Input.GetKey(KeyCode.A))
        {
            transform.Translate(Vector3.left * speedLeft1);
        }

    }

    void freezePosition()
    {
        if (Input.GetKey(KeyCode.Q))
        {
            freeze = true;
        }
        if (freeze == true)
        {
            rb.constraints = RigidbodyConstraints.FreezeAll;
        }
    }
}
