﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jump1 : MonoBehaviour {

    public bool grounded = false;
    public float Height1 = 10F; 
    Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        if (Input.GetKey(KeyCode.W))
        {
            if(grounded == true)
            {
                rb.velocity = new Vector3(0f, Height1); 
            }
        }
    }

    void OnTriggerEnter (Collider other)
    {
        grounded = true;
    }

    void OnTriggerExit (Collider other)
    {
        grounded = false;
    }
}
