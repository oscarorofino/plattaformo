﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour {

    Key myKey;

    void Start()
    {
        myKey = GameObject.FindWithTag("Key").GetComponent<Key>();
    }  
    void Update () {

        if(myKey.key == true)
        {
            transform.localScale = new Vector3(0f, 0f,0f);
        }
	}
}
