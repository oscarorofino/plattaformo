﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Polarity : MonoBehaviour {

    Movement1 myMovement1;
    Jump1 myJump1;

    Movement2 myMovement2;
    Jump2 myJump2;

    PlayerState1 myPlayerState1;
    PlayerState2 myPlayerState2;

	// Use this for initialization
	void Start () {
        myMovement1 = GameObject.FindWithTag("Player 1").GetComponent<Movement1>();
        myJump1 = GameObject.FindWithTag("Player 1").GetComponent<Jump1>();
        myPlayerState1 = GameObject.FindWithTag("Player 1").GetComponent<PlayerState1>();

        myMovement2 = GameObject.FindWithTag("Player 2").GetComponent<Movement2>();
        myJump2 = GameObject.FindWithTag("Player 2").GetComponent<Jump2>();
        myPlayerState2 = GameObject.FindWithTag("Player 2").GetComponent<PlayerState2>();
	}
	
	// Update is called once per frame
	void FixedUpdate ()
    {

	}
}
