﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement2 : MonoBehaviour {

    [SerializeField]
    public float speedLeft2;
    public float speedRight2;
    public bool ismoving2;
    public bool freeze = false;
    Rigidbody rb;
    void Start()
    {
        freezePosition();
        rb = GetComponent<Rigidbody>();
    }


    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.RightArrow))
        {
            transform.Translate(Vector3.right * speedRight2);
        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            transform.Translate(Vector3.left * speedLeft2);
        }

    }
    void freezePosition()
    {
        if (Input.GetKey(KeyCode.Q))
        {
            freeze = true;
        }
        if (freeze == true)
        {
            rb.constraints = RigidbodyConstraints.FreezeAll;
        }
    }
}
