﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jump2 : MonoBehaviour {

    public bool grounded = false;
    public float Height2 = 10F;
    Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        if (Input.GetKey(KeyCode.UpArrow))
        {
            if (grounded == true)
            {
                rb.velocity = new Vector3(0f, Height2);
            }
        }
    }

    void OnTriggerEnter(Collider other)
    {
        grounded = true;
    }

    void OnTriggerExit(Collider other)
    {
        grounded = false;
    }
}
