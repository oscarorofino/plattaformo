﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jump1 : MonoBehaviour {

    public float disToGround = 0.5f;
    public bool grounded;
    public int forceConst = 50;
    public Rigidbody rigid;
    public float jumpHeight = 10f;
    void imfalling()
    {transform.Translate(Vector3.down * 0.005f * Time.deltaTime);
    }
   void  isGrounded()
    {
        Physics.Raycast(transform.position, Vector3.down, disToGround);
        if (disToGround <= 0.5f)
        {
            grounded = true;
        }
    }

    void Start()
    {
        rigid = GetComponent<Rigidbody>();
        isGrounded();
        imfalling();
    }

    void Update()
    {

        if (Input.GetKey(KeyCode.W) && (grounded=true))
        {
            rigid.AddForce(Vector3.up * jumpHeight);
        }

    }

}
