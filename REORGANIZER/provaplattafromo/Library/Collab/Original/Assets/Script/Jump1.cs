﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jump1 : MonoBehaviour {
    [SerializeField]
    GameObject player1;
    public float disToGround = 0.5f;
    [SerializeField]
    public bool grounded;
    public int forceConst = 50;
    public Rigidbody rigid;
    public float jumpHeight = 10f;
    void imfalling()
    {transform.Translate(Vector3.down * 0.005f * Time.deltaTime);
    }

    private void OnCollisionEnter(Collision other)
    
    {
       

        if (other.gameObject.CompareTag("ground"))
        {
            grounded = true; // if the player is in the ground, the bool "canjump" become true
        }
        else
        {
            grounded = false; //if the player quit the ground, the bool become false
        }

    }
    void Start()
    {
        rigid = GetComponent<Rigidbody>();
        
        
        imfalling();
    }


    private void FixedUpdate()
    {
        if (Input.GetKey(KeyCode.W) && (grounded == true))
        {
            rigid.velocity = new Vector3(0f, jumpHeight, 0);
        }

    }

}
