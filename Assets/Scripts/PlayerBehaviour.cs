﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBehaviour : MonoBehaviour {

    public GameObject player2;

    public Vector2 speed;

    bool isMoving;
    bool isMovingLeft;
    bool isMovingRight;
    bool isJumping;
    public bool changePolarity;

    public float gravity;
    public float normalGravity;
    public float jumpHeight;
    public float normalJump;
    public float doubleJump;
    public float attractionSpeed;

    public LayerMask groundMask;

    Definitions.PlayerState state;
    public Definitions.Polarity polarity;

	// Use this for initialization
	void Start () {
        gravity = normalGravity;
        jumpHeight = normalJump;
        state = Definitions.PlayerState.idle;
        polarity = Definitions.Polarity.none;
        PlayerFall();

	}
	
	// Update is called once per frame
	void Update () {
        InputPlayer();
        UpdatePosition();
        UpdatePositionFromPolarity();  
    }

    void InputPlayer() {
        isMovingLeft = Input.GetKey(KeyCode.LeftArrow) && !Input.GetKey(KeyCode.RightArrow);
        isMovingRight = Input.GetKey(KeyCode.RightArrow) && !Input.GetKey(KeyCode.LeftArrow);
        isMoving = isMovingLeft || isMovingRight;
        isJumping = Input.GetKey(KeyCode.UpArrow);
        changePolarity = Input.GetKeyDown(KeyCode.RightControl);
    }

    void UpdatePosition() {
        Vector3 position = transform.localPosition;
        Vector3 scale = transform.localScale;
        if (changePolarity)
        {
            if (polarity == Definitions.Polarity.negative)
                polarity = Definitions.Polarity.none;
            else
                polarity++;
            ChangeColor();
        }
        if (isMoving) {
            if (isMovingLeft)
            {
                position.x -= speed.x * Time.deltaTime;
                scale.x = -1;
            }
            else if (isMovingRight)
            {
                position.x += speed.x * Time.deltaTime;
                scale.x = 1;
            }
            position = CheckWalls(position, scale.x);

        }
        if (isJumping && state != Definitions.PlayerState.jumping) {
            state = Definitions.PlayerState.jumping;
            speed = new Vector2(speed.x, jumpHeight);
        }
        if (state == Definitions.PlayerState.jumping) {
            position.y += speed.y * Time.deltaTime;
            speed.y -= gravity * Time.deltaTime;
        }
        if (speed.y <= 0)
            position = CheckGround(position);
        if (speed.y >= 0)
            position = CheckCeiling(position);
        transform.localPosition = position;
    }

    void UpdatePositionFromPolarity() {
        if (player2.GetComponent<Player2Behaviour>().polarity != Definitions.Polarity.none && polarity != Definitions.Polarity.none)
        {
            if (player2.GetComponent<Player2Behaviour>().polarity != polarity)
            {

                Vector2 direction = transform.position - player2.transform.position;
                float angle = Mathf.Atan2(direction.x, direction.y) * Mathf.Rad2Deg;
                transform.localPosition = Vector2.MoveTowards(transform.position, player2.transform.position, attractionSpeed * Time.deltaTime);
                transform.localPosition = CheckWalls(transform.localPosition, transform.localScale.x);
                jumpHeight = normalJump;
                gravity = 0;
            }
            else if (player2.GetComponent<Player2Behaviour>().polarity == polarity)
            {
                Vector2 direction = transform.position - player2.transform.position;
                float angle = Mathf.Atan2(direction.x, direction.y) * Mathf.Rad2Deg;
                transform.localPosition = Vector2.MoveTowards(transform.position, player2.transform.position, -attractionSpeed * Time.deltaTime);
                transform.localPosition = CheckWalls(transform.localPosition, transform.localScale.x);
                jumpHeight = doubleJump;
                gravity = normalGravity;
            }
        }
        else
        {
            if (changePolarity || player2.GetComponent<Player2Behaviour>().changePolarity)
            {
                jumpHeight = normalJump;
                gravity = normalGravity;
            }
        }
    }

    Vector3 CheckCeiling(Vector3 position) {

        Vector2 checkLeft = new Vector2(position.x - 0.5f, position.y + 0.5f);
        Vector2 checkMiddle = new Vector2(position.x, position.y + 0.5f);
        Vector2 checkRight = new Vector2(position.x + 0.5f, position.y + 0.5f);

        RaycastHit2D ceilingLeft = Physics2D.Raycast(checkLeft, Vector2.up, speed.y * Time.deltaTime, groundMask);
        RaycastHit2D ceilingMiddle = Physics2D.Raycast(checkMiddle, Vector2.up, speed.y * Time.deltaTime, groundMask);
        RaycastHit2D ceilingRight = Physics2D.Raycast(checkRight, Vector2.up, speed.y * Time.deltaTime, groundMask);

        if (ceilingLeft.collider != null || ceilingMiddle.collider != null || ceilingRight.collider != null) {
            RaycastHit2D hit = ceilingRight;
            if (ceilingLeft)
                hit = ceilingLeft;
            else if (ceilingMiddle)
                hit = ceilingMiddle;
            else if (ceilingRight)
                hit = ceilingRight;
            position.y = hit.collider.bounds.center.y - hit.collider.bounds.size.y / 2 - 0.5f;
            PlayerFall();//chiamo il metodo in modo che il player inizi a cadere dopo aver toccato una cella.
        }
        return position;
    }

    Vector3 CheckWalls(Vector3 position, float direction) {

        Vector2 checkUp = new Vector2(position.x + direction * 0.5f, position.y + 0.4f);
        Vector2 checkMiddle = new Vector2(position.x + direction * 0.5f, position.y);
        Vector2 checkBottom = new Vector2(position.x + direction * 0.5f, position.y - 0.4f);

        RaycastHit2D wallUp = Physics2D.Raycast(checkUp, new Vector2(direction, 0), speed.x * Time.deltaTime, groundMask);
        RaycastHit2D wallMiddle = Physics2D.Raycast(checkMiddle, new Vector2(direction, 0), speed.x * Time.deltaTime, groundMask);
        RaycastHit2D wallBottom = Physics2D.Raycast(checkBottom, new Vector2(direction, 0), speed.x * Time.deltaTime, groundMask);

        if (wallUp.collider != null || wallMiddle.collider != null || wallBottom.collider != null)
            position.x -= speed.x * Time.deltaTime * direction;

        return position;
    }

    Vector3 CheckGround(Vector3 position) {

        Vector2 checkLeft = new Vector2(position.x - 0.5f, position.y - 0.5f);
        Vector2 checkMiddle = new Vector2(position.x, position.y - 0.5f);
        Vector2 checkRight = new Vector2(position.x + 0.5f, position.y - 0.5f);

        RaycastHit2D groundLeft = Physics2D.Raycast(checkLeft, Vector2.down, speed.y * Time.deltaTime, groundMask);
        RaycastHit2D groundMiddle = Physics2D.Raycast(checkMiddle, Vector2.down, speed.y * Time.deltaTime, groundMask);
        RaycastHit2D groundRight = Physics2D.Raycast(checkRight, Vector2.down, speed.y * Time.deltaTime, groundMask);

        if (groundLeft.collider != null || groundMiddle.collider != null || groundRight.collider != null)
        {
            RaycastHit2D hit = groundRight;

            if (groundLeft)
                hit = groundLeft;
            else if (groundMiddle)
                hit = groundMiddle;
            else if (groundRight)
                hit = groundRight;
            state = Definitions.PlayerState.idle;
            speed.y = 0;
            position.y = hit.collider.bounds.center.y + hit.collider.bounds.size.y / 2 + 0.5f;
        }
        else {
            if (state != Definitions.PlayerState.jumping)
                PlayerFall();
        }
        return position;
    }

    void ChangeColor() {
        MeshRenderer renderer = GetComponent<MeshRenderer>();
        switch (polarity) {
            case Definitions.Polarity.none:
                renderer.material.color = Color.gray;                
                break;
            case Definitions.Polarity.positive:
                renderer.material.color = Color.red;
                break;
            case Definitions.Polarity.negative:
                renderer.material.color = Color.blue;
                break;
        }
    }

    void PlayerFall() {
        speed.y = 0;
        state = Definitions.PlayerState.jumping;
    }
}
