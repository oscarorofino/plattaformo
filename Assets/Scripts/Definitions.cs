﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Definitions : MonoBehaviour {

    public enum Polarity { none, positive, negative }

    public enum PlayerState { idle, moving, jumping }
}
